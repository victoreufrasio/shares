import boto3
import common
import json
import random
import time
from datetime import datetime


class ListOfProducts:
	def __init__(self):
		self.list = []

	def append(self, productInfo):
		self.list.append(productInfo)


	def get_urls_separated_by_comma(self):
		return ','.join([p.url for p in self.list])


class ProductInfo:
	def __init__(self, attributes):
		self.attributes = attributes
		self.url = common.get_product_url(self.name, self.productId)

	def get_name(self):
		return self.attributes.get('name')
	name = property(get_name)

	def get_productId(self):
		return self.attributes.get('productId')
	productId = property(get_productId)

	def get_ecommerceId(self):
		return self.attributes.get('ecommerceId')
	ecommerceId = property(get_ecommerceId)


class Share:
	def __init__(self, key, timestamp, **kwargs):
		self.ecommerceId , self.productId = key
		self.timestamp = timestamp
		for key, value in kwargs.items():
			setattr(self, key, int(value))
		self.updated = datetime.utcnow().strftime("%Y-%m-%d %H:%M:%S")

	def send_to_dynamo(self, dynamodb, shares_table_name):
		response = dynamodb.Table(shares_table_name).update_item(
			Key = {
				'productId': self.productId,
				'ecommerceId': self.ecommerceId
			},
			UpdateExpression = "SET shares = list_append(if_not_exists(shares, :empty_list), :r)",
			ExpressionAttributeValues = {
				":empty_list": [],
				":r": self.test()
			},
			ReturnValues="NONE"
		)

	def test(self):
		return [{
			'fb': self.facebook,
			'pi': self.pinterest,
			'li': self.linkedin,
			'gp': self.googleplus,
			't' : self.timestamp
		}]

	def __repr__(self):
		return "{0} {1} {2} {3} {4} {5} ".format(self.ecommerceId, self.productId, self.facebook, self.pinterest, self.linkedin, self.googleplus)


class Scan():
	def __init__(self, table):
		self.table = table

	def all(self, **kwargs):
		response = self.table.scan(**kwargs)
		while True:
			for row in response.get('Items'):
				yield row
			if response.get('LastEvaluatedKey'):
				response = self.table.scan(ExclusiveStartKey=response['LastEvaluatedKey'], **kwargs)
			else:
				break

	def all_chunked(self, chunk_size, **kwargs):
		response = self.table.scan(**kwargs)
		items = response.get('Items')
		while True:
			for chunk in [items[i:i+chunk_size] for i in range(0, len(items), chunk_size)]:
				yield chunk
			if response.get('LastEvaluatedKey'):
				response = self.table.scan(ExclusiveStartKey=response['LastEvaluatedKey'], **kwargs)
				items = response.get('Items')
			else:
				break	

class TokenManager():
	def __init__(self, filename):
		f = open(filename, 'r')
		self.USER_TOKENS = json.load(f)
		self.isInvalid = dict()

	def valid_token(self):
		while True:
			random.shuffle(self.USER_TOKENS)
			for token in self.USER_TOKENS:
				if token['token'] not in self.isInvalid:
					return token['token']
				else:
					diff = datetime.utcnow() - self.isInvalid[token['token']]
					if diff.total_seconds() > 1800:
						del self.isInvalid[token['token']]
						return token['token']
			time.sleep(1800)


	def invalidate_token(self, token):
		self.isInvalid['token'] = datetime.utcnow()