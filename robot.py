"""
	Curtix 2017
	Author: Paulo Victor Eufrasio <victoreufrasio at gmail dot com>

	Algorithm: 
	For each product in Product-Info table, fetch the number of times that
	product was shared in each of the following social networks: [facebook, googleplus,
	linkedin, mailru, pinterest, stumbleupon, vk].
"""
import boto3
import common
import socialparser
from boto3.dynamodb.conditions import Key, Attr
from classes import *
from datetime import datetime, timedelta
from socialparser import facebook, googleplus, linkedin, pinterest


dynamodb = boto3.resource("dynamodb",
	region_name='us-east-1',
	aws_access_key_id="AKIAJHC6YWGWPT4V22CA",
	aws_secret_access_key="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"
)
PRODUCT_INFO_TABLE_NAME = 'Product-Info_prod'
PRODUCT_SHARES_TABLE_NAME = 'Product-Shares_prod'

timeCalculate = datetime.today() - timedelta(days=2)
WEEK_AGO = timeCalculate.strftime("%Y-%m-%d") + ' 00:00:00'


def generator():
	productInfoTable = dynamodb.Table(PRODUCT_INFO_TABLE_NAME)
	fe = Attr('ecommerceId').eq('srv') & Attr('updated').gt(str(WEEK_AGO))
	for chunk in Scan(productInfoTable).all_chunked(50, FilterExpression=fe):
		products = ListOfProducts()
		for attributes in chunk:
			product = ProductInfo(attributes)
			products.append(product)
		yield products


def run():
	for listOfProducts in generator():
		"""
			Each socialparser get_shares returns a dictionary 
			{
				(ecommerceId-1, productId-1): numShares-1, 
				(ecommerceId-2, productId-2): numShares-2, 
				...
			}
		"""
		tm = TokenManager('tokens.json')
		facebook_shares   = socialparser.facebook.get_shares(listOfProducts, tm)
		pinterest_shares  = socialparser.pinterest.get_shares(listOfProducts)
		linkedin_shares   = socialparser.linkedin.get_shares(listOfProducts)
		googleplus_shares = socialparser.googleplus.get_shares(listOfProducts)

		all_shares = [facebook_shares, pinterest_shares, linkedin_shares, googleplus_shares]

		for key in set().union(*all_shares):
			share = Share(key=key,
				timestamp = datetime.utcnow().strftime("%Y-%m-%d"),
				facebook = facebook_shares.get(key, 0), 
				pinterest = pinterest_shares.get(key, 0), 
				linkedin = linkedin_shares.get(key, 0),
				googleplus = googleplus_shares.get(key, 0))
			share.send_to_dynamo(dynamodb, PRODUCT_SHARES_TABLE_NAME)


if __name__ == "__main__":
	run()
