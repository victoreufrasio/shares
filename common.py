from slugify import slugify


def get_product_url(ProductName, ProductId):
	r = slugify(ProductName + " " + ProductId)
	return "http://www.saraiva.com.br/" + r + ".html"