import json
import re
import urllib2


def get_shares(listOfProducts, tokenManager):
	token = tokenManager.valid_token()
	url = 'https://graph.facebook.com/?ids={0}&access_token={1}'.format(
		listOfProducts.get_urls_separated_by_comma(), 
		token
	)

	try:
		response = urllib2.urlopen(url)
	except:
		tokenManager.invalidate_token(token)
		get_shares(listOfProducts, tokenManager)
	else:
		return parse_shares(listOfProducts, response.read())

	
def parse_shares(listOfProducts, result):
	data = json.loads(result)
	toRet = dict()
	for productInfo in listOfProducts.list:
		if productInfo.url in data:
			v = data[productInfo.url]

			if "og_object" in v:
				if v["og_object"]['id'] != '10150130206654711':
					if 'share' in v:
						if 'share_count' in v["share"]:
							if v['share']['share_count'] > 0:
								toRet[(productInfo.ecommerceId, productInfo.productId)] = v["share"]['share_count']
	return toRet
