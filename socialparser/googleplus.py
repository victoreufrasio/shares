import json
import time
import urllib2


def get_shares(listOfProducts):
	toRet = dict()

	for productInfo in listOfProducts.list:	
		toRet.update( get_shares_single_product(productInfo) )
	
	return toRet


def get_shares_single_product(productInfo, numRetries=3):
	url = 'https://clients6.google.com/rpc?key=AIzaSyCKSbrvQasunBoV16zDH9R33D88CeLr9gQ'

	data = [{"method": "pos.plusones.get", "id": "p",
			 "params": {"nolog": "true", "id": productInfo.url, "source": "widget", "userId": "@viewer", "groupId": "@self"},
			 "jsonrpc": "2.0", "key": "p", "apiVersion": "v1"}]
	data = json.dumps(data)
	dataLenght = len(data)

	req = urllib2.Request(url, data, {'Content-Type': 'application/json', 'Content-Length': dataLenght})

	try:
		response = urllib2.urlopen(req)
		return parse_shares(productInfo, response.read())
	except:
		if numRetries > 0:
			time.sleep(1800)
			return get_shares_single_product(productInfo, numRetries-1)

		return {}
	

def parse_shares(productInfo, result):
	data = json.loads(result)

	if data[0]:
		if "result" in data[0]:
			if "metadata" in data[0]["result"]:
				if "globalCounts" in data[0]["result"]["metadata"]:
					if "count" in data[0]["result"]["metadata"]["globalCounts"]:
						if data[0]["result"]["metadata"]["globalCounts"]["count"] > 0:
							return {
								(productInfo.ecommerceId, productInfo.productId): data[0]["result"]["metadata"]["globalCounts"]["count"]
							}

	return {}
