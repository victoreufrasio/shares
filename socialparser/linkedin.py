import json
import time
import urllib2


def get_shares(listOfProducts):
	toRet = dict()

	for productInfo in listOfProducts.list:	
		toRet.update( get_shares_single_product(productInfo) )
	
	return toRet


def get_shares_single_product(productInfo, numRetries=3):
	url = 'https://www.linkedin.com/countserv/count/share?format=json&url={0}'.format(productInfo.url) 

	try:
		response = urllib2.urlopen(url)
		return parse_shares(productInfo, response.read())
	except:
		if numRetries > 0:
			time.sleep(1800)
			return get_shares_single_product(productInfo, numRetries-1)
		return {}


def parse_shares(productInfo, result):
	data = json.loads(result)

	if data.get('count', 0) > 0:
		return {
			(productInfo.ecommerceId, productInfo.productId): data['count']
		}

	return {}